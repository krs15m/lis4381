

# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### A1 # Requirements:

*Three Parts:*

1. Distrubuted Version Control with Git and Bitbucket
2. Development Installations
3. Questions

### README.md file should include the following items
	- Screenshot of AMPPS Installation My PHP Installation
	- Screenshot of running java Hello
	- Screenshot of running Android Studio - My First App
	- Git commands w/ short descriptions

> #### Git commands w/short descriptions:

1. git init - This command creates an empty git repository
2. git status - List the files you've changed and those you still need to add or commit
3. git add - Add one or more files to staging
4. git commit - Commit changes to head
5. git push - Send changes to the master branch of your remote repository
6. git pull - Fetch from and integrate with another repository or a local branch
7. git branch - List all the branches in your repo, and also tell you what branch you're currently in

#### Assignment Screenshots:

*Screenshot of AMPPS running My PHP Installation*

![Screenshot of AMPPS running My PHP Installation](img/php.PNG)

*Screenshot of running java Hello*:

![Screenshot of running java Hello](img/java.PNG)

*Screenshot of Android Studio - My First App*:

![Screenshot of Android Studio](img/hw.PNG)



