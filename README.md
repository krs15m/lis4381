# LIS4381

## Kenneth Sasser
### LIS4381 # Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Install JDK
	- Install Android Studio and create My First App
	- Provide screenshots of installations
	- Create BitBucket repo
	- Complete BitBucket Tutorial
	- Provide Git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create first user interface
	- Create second user interface
	- Provide screenshots of both interfaces running
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create database .mwb/.sql/ERD
	- Create mobile application
	- Provide Screenshots of ERD/App1st interface/App2nd interface
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Clone assignment starter files
	- Modify cloned repo and develop index.php
	- Develop Bootstrap Carousel
	- Demonstrate data validation via JavaScript
	- Create Favicon
	- (http://localhost/repos/lis4381 "Link to localhost repo")
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Create Server Side Validating form
	- Link Database to php files
	- Use Regular Expressions for validation
	- (http://localhost/repos/lis4381 "Link to localhost repo")
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Provide Screenshots of Application's 1st interface
	- Provide Screenshots of Application's 2nd interface
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Complete Server-side validation
	- Provide Regular Expression validation
	- Screenshots of P2, RSS Feed, Error catching, Editing
