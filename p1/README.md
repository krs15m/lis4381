# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### P1 # Requirements:
*Two Parts:*

1. Provide Screenshots of Application's 1st interface
2. Provide Screenshots of Application's 2nd interface

### README.md file should include the following items
	- Screenshot of running application's first user interface
	- Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of running application's first user interface*

![Screenshot of running application's first user interface](img/p11.PNG)

*Screenshot of running application's second user interface*

![Screenshot of running application's second user interface](img/p12.PNG)

