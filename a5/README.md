

# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### Assignment 5 Requirements:

*Three Parts*

1. Show PHP and MySQL mastery
2. Show Server Side validation mastery
3. Successfully deploy to BitBucket with localhost link


*First Application Screen:

![index.php](img/a51.png)

*Second Screenshot:

![Error](https://bitbucket.org/krs15m/lis4381/raw/395ee03289d8f810bad87d54c871fba7f49380af/a5/img/a52.PNG)

[Local Host](http://localhost/repos/lis4381)
