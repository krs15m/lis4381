# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### P2 # Requirements:
*Three Parts:*

1. Enable PHP and MySQL functionality
2. Enable Server-side validation
3. Implement RSS feed via PHP

### README.md file should include the following items
	- P2 main page
	- Edit capability
	- Error catching via Server-side validation
	- Homepage
	- RSS feed

#### Assignment Screenshots:

*P2 main page*

![P2 main page](img/thisisindex.png)

*Edit capability*

![Edit capability](img/edit.png)

*Error catching via Server-side validation*

![Edit capability](img/error.png)

*Homepage*

![Edit capability](img/home.png)

*RSS Feed*

![Edit capability](img/rss.png)