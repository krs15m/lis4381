# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### A2 # Requirements:
*Three Parts:*

1. Create first user interface
2. Create second user interface
3. Provide Screenshots of both user interfaces running

### README.md file should include the following items
	- Screenshot of running application's first user interface
	- Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of running application's first user interface*

![Screenshot of running application's first user interface](img/view.PNG)

*Screenshot of running application's second user interface*

![Screenshot of running application's second user interface](img/recipe.PNG)
