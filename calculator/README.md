# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### A4 # Requirements:
*Three Parts:*

1. Provide Screenshot of Landing Page in local repo
2. Provide Screenshot of Data Validation fail scenario
3. Provide Screenshot of Data Validation success scenario

### README.md file should include the following items
	- Screenshot of Landing Page
	- Screenshot of Data Validation fail scenario
	- Screenshot of Data Validation success scenario

#### Assignment Screenshots:

*Screenshot of Landing Page*

![Screenshot of Landing Page](https://bitbucket.org/krs15m/lis4381/raw/748ac76ac83d46d048c42f8e8da08d121be14700/a4/img/Capture1.PNG)

*Screenshot of Data Validation fail scenario*

![Screenshot of Data Validation fail scenario](img/Capture2.png)

*Screenshot of Data Validation success scenario*

![Screenshot of Data Validation success scenario](img/Capture3.PNG)

