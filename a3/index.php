<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Kenneth Sasser">
    <link rel="icon" href="../img/favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Requirements:</strong> 
					<br>- Develop Relational Database and provide ERD Screenshot
					<br>- Develop Ticket Sales Application and provide Screenshot
				</p>

				<h4>Petstore Database ERD</h4>
				<img src="img/a3.png" class="img-responsive center-block" alt="Petstore ERD">

				<h4>Android Studio Installation running first interface</h4>
				<img src="img/Capture1.png" class="img-responsive center-block" alt="1st interface running">
				<h4>Android Studio Installation running second interface</h4>
				<img src="img/Capture2.png" class="img-responsive center-block" alt="2nd interface running">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
