# LIS4381 - Mobile Web Application Development

## Kenneth Sasser

### A3 # Requirements:
*Three Parts:*

1. Create database .mwb/.sql/ERD
2. Create mobile application
3. Provide Screenshots of ERD/App1st interface/App2nd interface

### README.md file should include the following items
	- Screenshot of ERD
	- Screenshot of running application's first user interface
	- Screenshot of running application's second user interface

#### Assignment Screenshots:

*Screenshot of ERD*

![Screenshot of ERD](https://bitbucket.org/krs15m/lis4381/raw/235e7560525c2f15a57740bfeb537cf8fe2a9276/a3/img/a3.png)

*Screenshot of running application's first user interface*

![Screenshot of running application's first user interface](img/Capture1.PNG)

*Screenshot of running application's second user interface*

![Screenshot of running application's second user interface](img/Capture2.PNG)

